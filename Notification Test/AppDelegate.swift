//
//  AppDelegate.swift
//  Notification Test
//
//  Created by Carl Goldsmith on 15/11/2014.
//  Copyright (c) 2014 scrobby. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

